var setLayouts = debounce(function() {
	// do stuff
}, 250);



$(function(){
	FastClick.attach(document.body);
});

$(window).resize(setLayouts);

// debounce
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	}
}

// ref https://github.com/WICG/EventListenerOptions/pull/30
function isPassive() {
    var supportsPassiveOption = false;
    try {
        addEventListener("test", null, Object.defineProperty({}, 'passive', {
            get: function () {
                supportsPassiveOption = true;
            }
        }));
    } catch(e) {}
    return supportsPassiveOption;
}



//***************************************************
//          LAYOUT FUNCTIONALITY                    *
//***************************************************

var intervals = [], $b;


function initLayout() {
    $(function(){
        $b = $('body');
        setPages();
        onScroll();
    });
    $(window).scroll(function(){
        onScroll();
    });

}



function setPages() {
    intervals = [];
    var position = 0;
    $('.slide').each(function(){
        var height = $(this).outerHeight();
        intervals.push(position);
        position += height;
    });
}
function onScroll() {
    var top = $(window).scrollTop() + 1;
    var page = 0;
    var pageName = '';
    for (var i = 0; i < intervals.length; i++) {
        if (top >= intervals[i] && top < intervals[i+1] && !pageName) {

            pageName = 'page-'+page;
        } else if (typeof intervals[i+1] != 'undefined') {
            page++;
        }
    }
    if (!pageName) {
        pageName = 'page-'+page;
    }


    $b.removeAttr('class').addClass(pageName);
}





//--------CHECKBOX-----------------
$('.checkBox').click(function () {
    $(this).toggleClass('isChecked')
    $('.agreementText').toggleClass('isGreen')
})


//--------------SWIPER GALERY---------------
var swiper;
function initSwiper() {
    swiper = new Swiper('.swiper-container', {
        centeredSlides: true,
        loop: true
    });
}

initSwiper();




//------------MENU SCROOL---------------
$('.navLink').click(function (e) {
    e.preventDefault();
    const id = $(this).data('slide');
    $('.navLink').removeClass('isClicked');
    $('.main').removeClass('m-menuOn');
    $(this).addClass('isClicked');
    $("html, body").animate({ scrollTop: $(`#${id}`).offset().top }, 400);


});

//------------MOUSE POINTER ON GALLERY-------------------



    $(".swiper-container").mousemove(function( event ) {
        $(".cursor").css({ top: `${event.clientY}px`, left: `${event.clientX}px` }).text('Sekanti');
        $('.main').addClass('showCursor');


    }).mouseout(function () {
        $('.main').removeClass('showCursor');

    }).click(function () {
        swiper.slideNext();
    });



//------------MOUSE POINTER ON MAP BLOCK-------------------


    $("#map").mousemove(function( event ) {


        $(".cursor").css({ top: `${event.clientY}px`, left: `${event.clientX}px` }).text('Registruokitės');
        $('.main').addClass('showCursor');
        console.log(event.clientX,event.clientY)


    }).mouseout(function () {
        $('.main').removeClass('showCursor');
        console.log('out')

    }).click(function () {
        console.log('click')
        $("html, body").animate({ scrollTop: $('#3').offset().top }, 400);
    });



//------------MOUSE POINTER ON MAP BLOCK-------------------


    $(".contactsText a").mousemove(function( event ) {

        console.log(event,event.pageX)
        $(".cursor").css({ top: `${event.clientY}px`, left: `${event.clientX}px` }).text('Susisiekti');
        $('.main').addClass('showCursor');


    }).mouseout(function () {
        $('.main').removeClass('showCursor');
        console.log('out')

    });



//----------GOOGLE MAP--------------------

function initMap() {
    if (typeof lat != 'undefined' && lat) {
        var uluru = {lat: lat, lng: lng};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: uluru,
            styles: [{"featureType": "all", "elementType": "geometry.fill", "stylers": [{"visibility": "on"},{"color": "#f6f7fe"}]},{"featureType": "all", "elementType": "geometry.stroke", "stylers": [{"visibility": "on"},{"color": "#f6f7fe"}]},{"featureType": "all", "elementType": "labels.text.fill", "stylers": [{"color": "#1d30d7"}]},{"featureType": "all", "elementType": "labels.text.stroke", "stylers": [{"visibility": "on"},{"lightness": 16}]},{"featureType": "all", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]},{"featureType": "administrative", "elementType": "geometry.fill", "stylers": [{"color": "#f6f7fe"},{"lightness": 20}]},{"featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{"color": "#fefefe"},{"lightness": 17},{"weight": 1.2}]},{"featureType": "landscape", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"},{"lightness": 20}]},{"featureType": "landscape", "elementType": "geometry.fill", "stylers": [{"color": "#f6f7fe"}]},{"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#f5f5f5"},{"lightness": 21}]},{"featureType": "poi", "elementType": "geometry.fill", "stylers": [{"color": "#f6f7fe"}]},{"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#dedede"},{"lightness": 21}]},{"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"color": "#dbdff9"}]},{"featureType": "road", "elementType": "labels.text", "stylers": [{"color": "#1d30d7"},{"visibility": "off"}]},{"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"},{"lightness": 17}]},{"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#ffffff"},{"lightness": 29},{"weight": 0.2}]},{"featureType": "road.arterial", "elementType": "geometry", "stylers": [{"color": "#ffffff"},{"lightness": 18}]},{"featureType": "road.arterial", "elementType": "labels.text", "stylers": [{"color": "#e21212"},{"visibility": "off"}]},{"featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{"visibility": "on"},{"color": "#1d30d7"}]},{"featureType": "road.arterial", "elementType": "labels.text.stroke", "stylers": [{"visibility": "off"}]},{"featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]},{"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#ffffff"},{"lightness": 16}]},{"featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{"color": "#1d30d7"},{"visibility": "on"}]},{"featureType": "road.local", "elementType": "labels.text.stroke", "stylers": [{"visibility": "off"}]},{"featureType": "transit", "elementType": "geometry", "stylers": [{"color": "#f2f2f2"},{"lightness": 19}]},{"featureType": "transit", "elementType": "geometry.fill", "stylers": [{"color": "#ff0000"},{"visibility": "off"}]},{"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#e9e9e9"},{"lightness": 17}]},{"featureType": "water", "elementType": "geometry.fill", "stylers": [{"visibility": "on"},{"color": "#ffffff"}]}]
        });

        const markerWidth = $(this).width() / designWidth * 176;
    const markerHeight = $(this).width() / designWidth * 292;
        var mapicon = new google.maps.MarkerImage("img/pin.png", null, null, null, new google.maps.Size(markerWidth,markerHeight));
        var marker = new google.maps.Marker({
            position: uluru,
            map: map,
            icon: mapicon
        });
    }
}

//------MENIU BUTTON------------------

$('.menuBtn').click(function () {
    $('.main').toggleClass('m-menuOn');
    console.log('aa')
})


//-----------MOBILE LANDING PAGE FOOTER--------------
const footer = $('.footer').clone().addClass('footerCopy');
$('.m-page1').append(footer)




//-----------MEDIA QUERY FOR JS---------------

// media query event handler
if (matchMedia) {
    const mq = window.matchMedia("(min-width: 768px)");

    mq.addListener(WidthChange);
    WidthChange(mq);
}



// media query change
function WidthChange(mq) {
    if (mq.matches) {
        console.log('desktop');
        initLayout();
        designWidth = 2401;






    } else {
        console.log('mobile')
        if ( swiper !== undefined ) {
            swiper.destroy( true, true )
        }

        designWidth = 667;
        initLayout = null;

    }

}











